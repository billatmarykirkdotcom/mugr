#!/bin/bash

# Make sure Node is in the path
export PATH=$PATH:/usr/sbin

# Change to whatever directory this script file lives in
cd "$(dirname "$0")"

# Import our environment variable file and run index.js using node.
node index.js >> mugr.log 2>> mugr.err