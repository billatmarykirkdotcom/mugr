/* eslint-disable no-use-before-define */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
const { NodeSSH } = require('node-ssh');
const axios = require('axios');
const convert = require('xml-js');
const fs = require('fs');
const ini = require('multi-ini');
const https = require('https');
const SplunkLogger = require('splunk-logging').Logger;
const graylog2 = require('graylog2');
const snmp = require('net-snmp');

/*
index.js.

See: https://bitbucket.org/billatmarykirkdotcom/public/src/main/

Buckle up. Here's some Javascript. It's like a structured language with no boundaries.
Pascal with Aspergers. C with a randomly faulty keyboard. C++ with a serious brain injury.
On the bright side, it's not as insane as PERL or LISP.
Or Prolog. God. Those were wasted years.
Everything is a run-time error.

'And they call this progress'
-- Marvin the paranoid android.

Look, it said 'opinionated' on the tin.

Author: Bill Buchan  - bill at marykirk dot com.
Description: Short, Jockanese.
Age: Older than the hills. And feel older most days.
Location: Marykirk. I can see the pub from my window.
*/

// Globals so that we can pass polling information
// to the router call (and reduce log entries)
let stationHostName = '';
let stationPlatform = '';
let currentSpeed = -1;

// These will get populated by the INI file.
let config = null;
let configDefault = null;

// A sleep function. Because. We're in the real world. Sigh.
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// ----- Logging Stuff ------------------

// We hold our splunk logging object here.
let splunkLogger = null;
// And our graylog logging object here.
let graylogLogger = null;

// Log an info message to Splunk
function logSplunkInfo(msg) {
    if (!splunkLogger) return null;
    return splunkLogger.send({ message: msg, severity: 'info' });
}

// Log an info message to Graylog
function logGraylogInfo(msg) {
    if (!graylogLogger) return null;
    return graylogLogger.log(msg);
}

// Log a debug message to Graylog
function logGraylogDebug(msg) {
    if (!graylogLogger) return null;
    return graylogLogger.debug(msg);
}

// Log a debug message to Splunk and graylog
function debug(msg) {
    logSplunkInfo(msg);
    logGraylogDebug(msg);
    if (config.DEBUG === 'on') console.log(`   ${new Date().toISOString()}:     ${msg}`);
}

function logSplunkError(message) {
    if (!splunkLogger) return null;
    return splunkLogger.error(message);
}

function logGraylogError(message) {
    if (!graylogLogger) return null;
    return graylogLogger.error(message);
}

// Message is just a normal logging thing.
function message(message) {
    logSplunkInfo(message);
    logGraylogInfo(message);
    console.log(`   ${new Date().toISOString()}: ${message}`);
}

// Like Message, but indented a wee bit more. Handy for showing
// the less important things in life.
function message2(message) {
    logSplunkInfo(`    ${message}`);
    logGraylogInfo(`    ${message}`);
    console.log(`   ${new Date().toISOString()}:   ${message}`);
}

async function logSplunkObject(obj) {
    if (!splunkLogger) return;

    if (config.SHOW_SPLUNK_GRAPHING_OBJECT !== 'true') return;

    debug(`logSplunkObject: Sending object: ${JSON.stringify(obj)}`);
    await splunkLogger.send({ message: obj, severity: 'info' });
}

async function logGraylogObject(obj) {
    if (!graylogLogger) return;

    if (config.SHOW_GRAYLOG_GRAPHING_OBJECT !== 'true') return;

    debug(`logGraylogObject: Sending object: ${JSON.stringify(obj)}`);
    await graylogLogger.log(obj);
}

// We're passed an object which we'd like to commit to our log
// Such as a speed statistic from our radios.
async function logObject(obj) {
    // Are we logging to a file?
    if (config.LOG_STATION_STATS_TO_FILE) {
        fs.appendFile(config.LOG_STATION_STATS_TO_FILE, '\n', (err) => {
            // eslint-disable-next-line no-use-before-define
            if (err) logError(err);
        });
        fs.appendFile(config.LOG_STATION_STATS_TO_FILE, JSON.stringify(obj), (err) => {
            if (err) logError(err);
        });
    }
    await logSplunkObject(obj);
    await logGraylogObject(obj);
}

// We have something bad.
function logError(message, linkName) {
    console.error(`\n\n * ${new Date().toISOString()}: error: ${message}, linkName: ${linkName}\n`);
    if (linkName) {
        logSplunkError({ message, linkName });
        logGraylogError({ message, linkName });
    } else {
        logSplunkError(message);
        logGraylogError(message);
    }
}

// Set up our Splunk logging, if it's been configured.
function setUpSplunk() {
    try {
        // See if we're running Splunk
        if (config.SPLUNK_TOKEN !== '') {
            const host = config.SPLUNK_HOST;
            if ((!host) || (host === '')) {
                debug('SPLUNK_HOST not defined - not setting up Splunk logging');
                return;
            }

            const token = config.SPLUNK_TOKEN;
            if ((!token) || (token === '')) {
                debug('SPLUNK_TOKEN not defined - not setting up Splunk logging');
                return;
            }

            const splunkSettings = {
                token,
                url: host,
            };

            splunkLogger = new SplunkLogger(splunkSettings);

            message(`Logging to splunk server on: ${host}`);
        }
    } catch (error) {
        logError(`setUpSplunk: Error: ${error}`);
        logError(error.stack);
        process.exit(1);
    }
}

// Set up our Graylog logging, if it's been configured.
function setUpGraylog() {
    try {
        // See if we're running Graylog
        const graylogHost = config.GRAYLOG_HOST;
        if ((!graylogHost) || (graylogHost === '')) {
            debug('GRAYLOG_HOST not defined - not setting up Graylog logging');
            return;
        }
        const graylogConfig = {
            servers: [
                {
                    host: graylogHost,
                    port: 12201,
                }],
            // the name of this host - (optional, default: os.hostname())
            // 'hostname': hostname,

            // the facility for these log messages
            // (optional, default: "Node.js")
            facility: 'mugr',

            // max UDP packet size, should never exceed the
            // MTU of your system (optional, default: 1400)
            bufferSize: 1350,
        };
        message(`Graylog logging config: ${JSON.stringify(graylogConfig)}`);
        // eslint-disable-next-line new-cap
        graylogLogger = new graylog2.graylog(graylogConfig);

        message(`Logging to graylog server on: ${graylogHost}`);
    } catch (error) {
        logError(`setUpGraylog: Stack: ${error.stack}`);
        process.exit(1);
    }
}
// ---- End of Logging Stuff --------------

// Lets not write indesciperable numbers. Lets pretend we
// have a -h flag permanently set on. Age does this to you.
// incoming is in Kbytes..
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'];

    const i = Math.floor(Math.log(bytes * k) / Math.log(k));

    const res = `${parseFloat(((bytes * k) / (k ** i)).toFixed(dm))} ${sizes[i]}`;
    // debug(`converted: ${bytes} to ${res}`);
    return res;
}

// Replace the tokens in the incoming string with the values
// from the object. Kinda like what the `${fieldName}` stuff does
// but with a string we pluck from the INI file.
function replaceTokens(inString, obj) {
    let outString = inString;
    const keyNames = Object.keys(obj);
    for (let i = 0; i < keyNames.length; i++) {
        const tk = keyNames[i];
        let replacement = obj[tk];
        if (tk === 'currentSpeed') replacement = formatBytes(replacement);
        outString = outString.replace(`\${${tk}}`, replacement);
    }
    // debug(`Converted string: ${inString} to ${outString}`);
    return outString;
}

// Publish a message to slack.
async function publishSlackMsg(msg) {
    const slackToken = config.SLACK_TOKEN;

    if ((!slackToken) || (slackToken === '')) {
        debug('No Slack token defined using SLACK_TOKEN - not doing Slack');
        return false;
    }
    const channel = config.SLACK_CHANNEL;
    if ((!channel) || (channel === '')) {
        debug('No Slack channel defined using SLACK_CHANNEL - not doing Slack');
        return false;
    }

    try {
        debug(`Posting slack msg: ${msg} to channel ${channel}`);

        const url = 'https://slack.com/api/chat.postMessage';
        const res = await axios.post(url, {
            channel,
            text: msg,
        }, { headers: { authorization: `Bearer ${slackToken}` } });

        if (res.data) {
            debug(JSON.stringify(res.data));
            if (res.data.ok) {
                debug(`Posting message: ${msg} to Slack channel ${channel} has succeeded`);
                return true;
            }
            logError(`Posting to Slack has failed. Message is: ${res.data.error}`);
            return false;
        }
        return false;
    } catch (error) {
        logError('publishSlackMsg: Error');
        logError(JSON.stringify(error));
        return false;
    }
}

// Check the link object and the config object for the properties, and if they don't exist,
// (and we've defined an error message), hard halt with an error message. This keeps
// crap from the INI file crashing us later.
async function checkParams(l, paramName, errorMessage) {
    async function haltWithForce(message) {
        logError(message);
        await sleep(1000);
        process.exit(1);
    }

    try {
        if (l === null) await haltWithForce('The Link object isn\'t defined');
        if (config === null) await haltWithForce('The Config object isn\'t defined');
        if (configDefault === null) await haltWithForce('The Default Config object isn\'t defined');

        let res = l[paramName];
        if ((!res) || (res === '')) {
            res = configDefault[paramName];
        }

        if ((!res) || (res === '')) {
            if (errorMessage !== '') {
                await haltWithForce(errorMessage);
            }
        }

        return res;
    } catch (error) {
        haltWithForce(`checkParams: Error: ${error.stack}`);
        return null;
    }
}

// This creates our SSH Config object by getting values from the INI file.
// The radio SSH passes in isSource=true, and the router passes in isSource=false
// SOURCE tends to be the radios, and TARGET tends to be the router.
// Pick up
//   USERNAME - the username
//   PASSWORD - the password
//   IP - the target IP address
//   PORT - the ssh port number
//
async function getSSHConfig(l, linkName, isSource) {
    let t = 'SOURCE';
    if (!isSource) t = 'TARGET';

    const res = {
        host: await checkParams(l, `${t}_IP`, `Please define the ${t}_IP address of the link in section: ${linkName}`),
        username: await checkParams(l, `${t}_USERNAME`, `Please define the ${t}_USERNAME for the device in section: ${linkName}`),
        port: parseInt(await checkParams(l, `${t}_PORT`, `Please define the target SSH port using ${t}_PORT in section: ${linkName}`), 10),
        readyTimeout: 15000,
        compress: false,
    };

    // Lets not dump passwords to our log..
    debug(`SSH Config: ${JSON.stringify(res)}`);
    res.password = await checkParams(l, `${t}_PASSWORD`, `Please define the ${t}_PASSWORD for the device in section: ${linkName}`);
    return res;
}

// mikrotikNumber - convert "2.3Gbs" to a Kilobits value.
function mikrotikNumber(original) {
    const i = original;

    const ending = i.substr(i.length - 4, 4);
    const thisVal = i.substr(0, i.length - 4);
    let n = Number(thisVal);

    switch (ending) {
    case 'Kbps':
        break;
    case 'Mbps':
        n *= 1024;
        break;
    case 'Gbps':
        n *= (1024 * 1024);
        break;
    default:
        logError(`mikrotikNumber: Found a strange ending: ${ending}`);
    }

    // debug('Broken down string: ' + i + 'to: ' + thisVal + ' and: '
    // + ending + ', and produced number: ' + n + ' Kbps');
    return n;
}

// Log into our W60G and get the 'status' page for the W60g station by running:
//        /interface w60g station monitor wlan60-station-1 once without-paging
// From that page, pick up the capacity of the link to the connected station,
// returning a number representing kilobits per second.
// return 0 if there is not a connection
// return -1 if there was some other error preventing us getting a speed.
async function getSpeedW60G(l, linkName) {
    let conn = null;
    const ssh = new NodeSSH();

    try {
        const sshConfig = await getSSHConfig(l, linkName, true);

        conn = await ssh.connect(sshConfig);

        if (!conn) {
            logError(
                `I cannot log into the W60G using IP: ${sshConfig.host} and username: ${sshConfig.username} for link: ${linkName}`,
                linkName);
            return -1;
        }
    } catch (error) {
        logError("getSpeedW60G: The back end didn't respond to the login request... Is the IP, username and password for the W60G correct?", linkName);
        logError(`getSpeedW60G: Stack: ${error.stack}`);
        return -1;
    }

    let sshReturn = null;

    try {
        sshReturn = await ssh.execCommand(':put [/system identity get name]');

        stationHostName = sshReturn.stdout;
        debug(`W60G hostname: ${stationHostName}`);
    } catch (error) {
        logError("getSpeedW60G: The back end didn't respond to the /system identity request", linkName);
        logError(`getSpeedW60G: Stack: ${error.stack}`);
        if (sshReturn && sshReturn.length > 0) logError(JSON.stringify(sshReturn));
        return -1;
    }

    let sshStats = null;
    let sshStatsFull = null;
    try {
        /*
            If there's no connection:
            connected: no
        */

        /*
            /interface w60g station monitor wlan60-station-1 once without-paging
            connected: yes
            remote: 04:D6:AA:76:BE:20
            tx-mode: dmg
            tx-mcs: 8
            tx-phy-rate: 2.3Gbps
            signal: 80
            rssi: -60
            tx-sector: 45
            tx-sector-info: right 0.6 degrees, up 0.6 degrees
            distance: 283.3m
        */

        sshStatsFull = await ssh.execCommand('/interface w60g station monitor wlan60-station-1 once without-paging ');
        sshStats = sshStatsFull.stdout;

        if (sshStats.length < 1) {
            message2(`The station is not connected - got error: ${sshStatsFull.stderr}`);
            return Number(0);
        }
    } catch (error) {
        logError("getSpeedW60G: The back end didn't respond to the /interface w60g station monitor wlan60-station-1 command.", linkName);
        logError(`getSpeedW60G: Stack: ${error.stack}`);
        if (sshStatsFull && sshStatsFull.length > 0) logError(JSON.stringify(sshStatsFull));
        return -1;
    }

    // Basically try and read all that crap into a dictionary
    // so we can easily pull out the stuff we care about later.
    try {
        const sshStatsA = sshStats.split('\r\n');

        // debug('Array split: ' + JSON.stringify(sshStatsA));

        let mySpeed = -1;
        let amConnected = false;

        for (let i = 0; i < sshStatsA.length; i++) {
            const line = (`${sshStatsA[i]}`).toString().trim();
            // debug('Processing line: ' + line);

            if (line.length > 0) {
                const lineA = line.split(':');

                // debug('LineA: ' + JSON.stringify(lineA));
                if (lineA.length > 0) {
                    const key = lineA[0].toString().trim();
                    // reassemble the value if we got a little carried away splitting by semicolon.
                    let thisVal = '';
                    for (let j = 1; j < lineA.length; j++) {
                        if (thisVal !== '') thisVal += ':';
                        thisVal += lineA[j].toString().trim();
                    }

                    if (config.SHOW_STATION_STATS === 'true') {
                        debug(`W60G Status: ${key}=${thisVal}`);
                    }
                    if (key === 'tx-phy-rate') {
                        // Convert 2.3gbs into a workable number
                        mySpeed = mikrotikNumber(thisVal);
                    }
                    if (key === 'connected') {
                        if (thisVal === 'no') {
                            amConnected = false;
                        } else {
                            amConnected = true;
                        }
                    }
                }
            }
        }

        // If we don't have a connection, fake the speed to zero
        // so at least we get some logging.
        if (amConnected === false) {
            mySpeed = 0;
        }

        debug(`Speed: ${formatBytes(mySpeed)}`);

        // Log out of the device.
        await ssh.execCommand('exit');

        if (mySpeed > -1) {
            await logObject({
                linkName,
                type: 'W60G',
                AP: stationHostName,
                speed: mySpeed,
                on: new Date(),
            });
            return (mySpeed);
        }

        debug('I failed to parse the returned structure from the W60G:');
        debug(JSON.stringify(sshStatsFull));

        return -1;
    } catch (error) {
        logError('getSpeedW60G: We failed whilst parsing the returned value. ', linkName);
        logError(`getSpeedW60G: Stack: ${error.stack}`);
        return -1;
    }
}

// Log into our AF60LR and get the 'status' page - same as running https://myAccessPoint/status.cgi
// From that page, pick up the capacity of the link to the connected station,
// returning a number representing kilobits per second (of combined capacity)
// return 0 for there is not a connection
// return -1 if there was some other error preventing us getting a speed.

async function getSpeedAF60LR(l, linkName) {
    let sshReturn = null;
    try {
        const ssh = new NodeSSH();

        const sshConfig = await getSSHConfig(l, linkName, true);
        const conn = await ssh.connect(sshConfig);

        if (!conn) {
            logError(`I cannot log into the AF60LR using IP: ${sshConfig.host} and username: ${sshConfig.username} for link: ${linkName}`, linkName);
            return -1;
        }

        sshReturn = await ssh.execCommand('ubntbox status.cgi');
        if (sshReturn.stdout === 'bad command name ubntbox (line 1 column 1)') {
            // For some reason, some gigabeams at 1.2.0 firmware dont have ubntbox???
            logError(`Failed to run ubntbox on host: ${sshConfig.host} - ${sshReturn.stdout}. Is this actually a UBIQUITI device?`, linkName);
            return -1;
        }
        const { stdout, stderr } = sshReturn;

        if (!stdout) {
            logError('The AF60LR isnt responding to SSH commands.', linkName);
            return -1;
        }

        const chunks = stdout.split('\n');
        if ((chunks[0].length < 1) || (sshReturn.stderr !== '')) {
            logError(`I didn't get a valid response from the command 'ubntbox status.cgi - got ${stdout}, stderr: ${stderr}`, linkName);
            return -1;
        }
        const status = JSON.parse(chunks[chunks.length - 1]);

        stationHostName = status.host.hostname;
        stationPlatform = status.host.devmodel;

        debug(`Logged into radio: ${stationHostName}, platform: ${stationPlatform}, using IP address: ${sshConfig.host}`);

        // Now lets go digging down into the JSON structure...

        const { wireless } = status;
        if (!wireless) {
            logError('The returned JSON structure is unrecognised - is this an AF60LR?', linkName);
            return Number(-1);
        }

        // eslint-disable-next-line camelcase
        const { prs_info } = wireless;

        const stations = wireless.sta;
        if (!stations) {
            logError('The returned JSON structure is unrecognised - is this an AF60LR?', linkName);
            return Number(-1);
        }
        const sta = stations[0];

        if (sta === undefined) {
            message2('The remote station is NOT connected - returning ZERO');
            return Number(0);
        }

        if (config.SHOW_STATION_STATS === 'true') {
            debug(`Found station stats: ${JSON.stringify(status)}`);
        }
        let mySpeed = Number(sta.prs_sta.capacity);

        // Any platform specific tweaks.
        switch (stationPlatform) {
        case 'airFiber 60 LR':
            break;
        case 'GigaBeam LR':
            mySpeed *= (1024); // Yup. Gigabeam LR's return bbs not Kbps.
            break;
        default:
            break;
        }
        debug(`Speed: ${formatBytes(mySpeed)}`);

        await logObject({
            linkName,
            AP: stationHostName,
            type: 'AF60LR',
            platform: stationPlatform,
            speed: mySpeed,
            ssid: wireless.essid,
            frequency: prs_info.frequency,
            on: new Date(),
        });

        // The Ubiquiti GB's and AF60LR's seem to lose their SSH
        // servers if you don't properly drop the sessions.
        await ssh.execCommand('exit');

        return (mySpeed);
    } catch (error) {
        logError("getSpeedAF60LR: The back end didn't respond to the status request... Is the IP, username and password for the AFL60LR correct?", linkName);
        if (sshReturn) {
            logError(`sshReturn stdout is: ${sshReturn.stdout}, stderr is: ${sshReturn.stderr}`);
        }
        logError(`getSpeedAF60LR: Stack: ${error.stack}`);
        return -1;
    }
}

// Scrape the link info from our Mimosa B5 and use the rest interface (which you have to enable)
// and get the Link status object (as XML)
// From that object, pick up the capacity of the link to the connected station,
// returning a number representing kilobits per second.
// return 0 if there is not a connection
// return -1 if there was some other error preventing us getting a speed.
async function getSpeedB5(l, linkName) {
    try {
        // We only ever will read speed from this, so it'll always be the source.
        const t = 'SOURCE';
        const username = await checkParams(l, `${t}_USERNAME`, `Please define the ${t}_USERNAME for the device in section: ${linkName}`);
        const password = await checkParams(l, `${t}_PASSWORD`, `Please define the ${t}_PASSWORD for the device in section: ${linkName}`);

        const url = `https://${l.SOURCE_IP}/core/api/service/status?username=${username}&password=${password}`;

        // Ignore SSL errors at request level
        const agent = new https.Agent({ rejectUnauthorized: false });

        const response = await axios.get(
            url,
            {
                httpsAgent: agent,
                headers: {
                    'content-type': 'application/xml',
                    Accept: 'application/xml',
                    'Cache-Control': 'no-cache',
                },
            },
        );

        let mySpeed = 0;

        // Yup. Pulling in XMS, converting to JSON.
        // I need a shower.
        const results = convert.xml2json(response.data, { compact: true, spaces: 4 });
        // console.log('results', results);

        // Oh lets use JSON they said, it'd be easy, they said.. Sigh.
        const p1 = JSON.parse(results);
        if (p1) {
            // console.log('p1', p1);
            const p2 = p1.response;
            if (p2) {
                // console.log('p2', p2);
                const p3 = p2.mimosaContent;
                if (p3) {
                    // console.log('p3', p3);
                    const p4 = p3.values;
                    if (p4) {
                        // console.log('p4', p4);

                        /*
                        * XML -> JSON leaves such a lot of ugly artifacts. Such as this:
                        p4 {
                            SignalStrength: { _text: ' -62.6394' },
                            TxRate: { _text: ' 270' },
                            RxRate: { _text: ' 400' },
                            Noise: { _text: ' -89.949' },
                            Chains_1_2: { _text: ' 5680-5720 MHz ' },
                            Chains_3_4: {},
                            Tx_Power: { _text: ' 0' },
                            Tx_Phys_Rate: { _text: ' 270' },
                            Rx_Phys_Rate: { _text: ' 400' },
                            Rx_MCS: { _text: ' 9' },
                            Details: { _ELEMENT: [ [Object], [Object], [Object], [Object] ] },
                            Noise2: {}
                        }
                        */

                        if (config.SHOW_STATION_STATS === 'true') {
                            debug(`Object supplied by Mimosa: ${JSON.stringify(p4)}`);
                        }

                        // eslint-disable-next-line dot-notation
                        const p5 = p4['TxRate'];
                        if (p5) {
                            // console.log('p5', p5);

                            // Mb -> Kb
                            // eslint-disable-next-line dot-notation
                            const p6 = Number(p5['_text']) * 1024;
                            if (p6) {
                                mySpeed = p6;
                            }
                        }
                    }
                }
            }
        }

        debug(`Speed: ${formatBytes(mySpeed)}`);

        if (mySpeed > -1) {
            await logObject({
                linkName,
                type: 'B5',
                AP: stationHostName,
                speed: mySpeed,
                on: new Date(),
                /*
                * We'd have to run another call to the device to get this information.
                * And since we're doing this every minute, I figure that
                * this is too expensive in terms of CPU and time for
                * basically static information.
                AP: stationHostName,
                platform: stationPlatform,
                ssid: wireless.essid,
                frequency: prs_info.frequency,
                */
            });
        }
        return (mySpeed);
    } catch (error) {
        logError(`getSpeedB5: We failed whilst parsing the returned value. ${error}`, linkName);
        return -1;
    }
}

// net-snmp isnt promised, so this wrappers the callback...
async function snmpWrapper(session, thisOid, numberRange, linkName) {
    // eslint-disable-next-line no-new
    let mySpeed = 0;
    const res = new Promise((resolve) => {
        debug(`looking for oidTarget: ${thisOid}`);
        session.get([thisOid], (error, varbinds) => {
            if (error) {
                console.error(error.stack);
                logError(error.stack);
                session.close();
                return -1;
            }
            debug(`getSpeedSNMP: Got object: ${JSON.stringify(varbinds)}`);
            const rateVar = varbinds[0];
            if (rateVar) {
                mySpeed = rateVar.value;
                switch (numberRange) {
                case 'B':
                    // We're returning Kb
                    mySpeed /= 1024;
                    break;
                case 'K':
                    break;
                case 'M':
                    mySpeed *= 1024;
                    break;
                case 'G':
                    mySpeed *= (1024 * 1024);
                    break;
                default:
                    break;
                }
                debug(`Link Carrier Speed: ${formatBytes(mySpeed)}`);

                if (mySpeed > -1) {
                    logObject({
                        linkName,
                        type: 'SNMP',
                        speed: mySpeed,
                        on: new Date(),
                    });
                }
                resolve(mySpeed);
            }
        });
    });
    // We're just using res as a promise-complete indicator.
    await res;
    return mySpeed;
}

// Scrape the link info from our target using SNMP
// returning a number representing kilobits per second.
// return 0 if there is not a connection
// return -1 if there was some other error preventing us getting a speed.
async function getSpeedSNMP(l, linkName) {
    try {
        // We only ever will read speed from this, so it'll always be the source.
        const t = 'SOURCE';
        const oidsString = await checkParams(l, `${t}_OIDS`, `Please define the ${t}_OIDS for the device in section: ${linkName}`);
        const community = await checkParams(l, `${t}_SNMP_COMMUNITY`, `Please define the ${t}_SNMP_COMMUNITY for the device in section: ${linkName}`);

        if (!oidsString || !community) return -1;
        const oids = oidsString.split(',');
        const oidTable = [];
        // Now overwrite each oid with the INI file variable
        for (let i = 0; i < oids.length; i++) {
            const name = oids[i];
            const numberRange = name.substr(-1, 1);
            const oid = await checkParams(l, oids[i], `Please define the variable ${oids[i]}_used for OID specification in section: ${linkName}`);
            oidTable.push({
                name,
                oid,
                numberRange,
            });
        }

        // At this point, I'm just taking the first Oid...
        const thisOid = oidTable[0];
        const { name, oid, numberRange } = thisOid;

        debug(`getSpeedSNMP: Connecting to: ${l.SOURCE_IP} using community: ${community} and searching for OIDS: '${name}'  '${oid}' using number range: ${numberRange}`);

        const session = snmp.createSession(l.SOURCE_IP, community);
        const mySpeed = snmpWrapper(session, oid, numberRange, linkName);
        await mySpeed;
        session.close();
        return (mySpeed);
    } catch (error) {
        logError(`getSpeedSNMP: We failed whilst parsing ${linkName} - the returned value. ${error.stack}`);
        return -1;
    }
}

// Log into our router and set our VLAN interface on or off,
// depending on the state of our 'setToDisabled' flag.
// TRUE means we want it disabled, FALSE means we want it enabled.
// If this seems about-face - the Mikrotik CLI uses a 'disabled' flag instead of an 'enabled' flag.
// return 'false' if we've failed, true if we succeeded.
async function setPort(l, linkName, setToDisabled) {
    try {
        const ssh = new NodeSSH();
        const sshConfig = await getSSHConfig(l, linkName, false);
        const conn = await ssh.connect(sshConfig);

        if (!conn) {
            logError(`I cannot log into the router using IP: ${sshConfig.host} and username: ${sshConfig.username} for link: ${linkName}`);
            return -1;
        }

        // get the hostname:
        const hostnameSta = await ssh.execCommand(':put [/system identity get name]');
        const hostname = hostnameSta.stdout;

        debug(`Logged into router: ${hostname}`);

        const vlanInterface = await checkParams(l, 'TARGET_INTERFACE', `Please define the TARGET_INTERFACE in section: ${linkName}`);
        const force = await checkParams(l, 'TARGET_FORCE', `Please define TARGET_FORCE in section: ${linkName}`);

        // this returns true or false - true if the interface is DISABLED
        const cmd = `:put [/interface vlan get [find name="${vlanInterface}"] disabled] `;

        debug(`setPort: Running command: ${cmd}`);

        const sshReturn = await ssh.execCommand(cmd);
        let currentStatus = false;
        debug(`  and got result: ${sshReturn.stdout}`);

        if (sshReturn.stdout === 'no such item') {
            // We have an error with our interface name. Abort.
            logError(`Router ${sshConfig.host} interface ${vlanInterface} does not exist`);
            await publishSlackMsg(`Error: Link: ${sshConfig.host} interface ${vlanInterface} - incorrect configuration. The interface does not exist.`);
            return true;
        }
        if (sshReturn.stdout === 'true') currentStatus = true;

        debug(`setPort: Current DISABLED status is: ${currentStatus} (${sshReturn.stdout})`);
        if (currentStatus === setToDisabled) {
            message2(`Port: ${vlanInterface} DISABLED=${currentStatus} on router: ${hostname} and we want DISABLED=${setToDisabled} - Nothing to do.`);

            // Work out if we want to ignore the current settings
            // and just force it to perform the update.
            // Handy for debugging.
            let setForce = false;
            if (force === 'true') setForce = true;

            if (!setForce) {
                return true;
            }
            debug('FORCE is enabled - running the update command');
        }

        let newFlag;
        if (!setToDisabled) {
            // Enable the port. Yes. It's about face.
            newFlag = 'no';
        } else {
            // Disable the port. Yes. It's about face.
            newFlag = 'yes';
        }
        const setCmd = `/interface vlan set [find name="${vlanInterface}"] disabled=${newFlag}`;

        debug(`Running change command: ${setCmd}`);

        const setReturn = await ssh.execCommand(setCmd);

        debug(JSON.stringify(setReturn));
        let success = false;
        if (setReturn.stdout === '') success = true;

        if (success) {
            let successMsg = '';

            // If we have alternative SLACK messages, send them.
            if (config.SLACK_ENABLE_LINK_MESSAGE) {
                // 'no' is we've successfully enabled the port...
                if (newFlag === 'no') {
                    successMsg = replaceTokens(config.SLACK_ENABLE_LINK_MESSAGE,
                        {
                            vlanInterface,
                            currentStatus,
                            hostname,
                            stationHostName,
                            currentSpeed,
                            newFlag,
                        });
                } else {
                    successMsg = replaceTokens(config.SLACK_DISABLE_LINK_MESSAGE,
                        {
                            vlanInterface,
                            currentStatus,
                            hostname,
                            stationHostName,
                            currentSpeed,
                            newFlag,
                        });
                }
            } else {
                successMsg = `Port: ${vlanInterface} DISABLED=${currentStatus} on router: ${hostname
                } for link: ${stationHostName} running speed: ${formatBytes(currentSpeed)} successfully set to: ${newFlag}`;
            }

            await publishSlackMsg(successMsg);
            message(successMsg);
            return true;
        }

        let failMsg = '';
        if (config.SLACK_FAIL_MESSAGE) {
            failMsg = replaceTokens(config.SLACK_FAIL_MESSAGE,
                {
                    vlanInterface, currentStatus, hostname, stationHostName, currentSpeed, newFlag,
                });
        } else {
            failMsg = `Port: ${vlanInterface} DISABLED=${currentStatus} on router: ${hostname
            } for link: ${stationHostName} running speed: ${formatBytes(currentSpeed)} FAILED to set:: ${newFlag}`;
        }

        await publishSlackMsg(failMsg);
        logError(failMsg);
        logError(`Failed to run command: ${setCmd}`);
        logError(JSON.stringify(setReturn));
        return false;
    } catch (error) {
        logError(`setPort: ssh error: ${error}`);
        logError(error.stack);
        return false;
    }
}

async function poll() {
    try {
        // Read the 'index.ini' file
        const content = ini.read('index.ini');
        config = content.config;
        configDefault = content.default;

        // Set up our logging..
        setUpSplunk();
        setUpGraylog();

        debug(`"config" section: ${JSON.stringify(config)}`);
        debug(`"default" section: ${JSON.stringify(configDefault)}`);

        // Now process each link section from the INI file
        // eslint-disable-next-line no-restricted-syntax
        for (const linkName in content) {
            if ((linkName !== 'config') && (linkName !== 'default')) {
                const thisLink = content[linkName];
                message(`Processing Link: ${linkName}`);

                debug(`link configuration: ${JSON.stringify(thisLink)}`);

                // eslint-disable-next-line no-await-in-loop
                const tsString = await checkParams(thisLink, 'MIN_SPEED', `Please define the MINIMUM_SPEED for the device in section: ${linkName}`);
                const targetSpeed = Number(tsString);

                // eslint-disable-next-line no-await-in-loop
                const thisType = await checkParams(thisLink, 'SOURCE_TYPE', `Please define the SOURCE_TYPE for the device in section: ${linkName}`);

                debug(`Processing INI object: ${linkName}: ${JSON.stringify(thisLink)}`);
                // get speed from source device

                // The global variable currentSpeed holds the current link speed.
                // -1 means error,
                // 0 means disconnected,
                // and a positive is the speed in Kbps. EG 150000 = 150Mbps, 150 = 150Kbps, etc.
                // (It's a global because I re-use it in a logging message in setPort. Yes. Icky).
                currentSpeed = -1;

                // Depending on the type of radio, go get the current link speed
                // debug('Poll: choosing radio based on: ' + JSON.stringify(thisType));
                switch (thisType) {
                case 'AF60LR':
                    // eslint-disable-next-line no-await-in-loop
                    currentSpeed = await getSpeedAF60LR(thisLink, linkName);
                    break;
                case 'W60G':
                    // eslint-disable-next-line no-await-in-loop
                    currentSpeed = await getSpeedW60G(thisLink, linkName);
                    break;
                case 'B5':
                    // eslint-disable-next-line no-await-in-loop
                    currentSpeed = await getSpeedB5(thisLink, linkName);
                    break;
                case 'SNMP':
                    // eslint-disable-next-line no-await-in-loop
                    currentSpeed = await getSpeedSNMP(thisLink, linkName);
                    break;
                default:
                    logError(`Please set SOURCE_TYPE to either W60G, B5, SNMP or AF60LR in section: ${linkName}`);
                    process.exit(1);
                    break;
                }

                if ((currentSpeed < 0) || (currentSpeed === undefined)) {
                    logError(`Failed to get speed from source device in section: ${linkName}`);
                } else {
                    // If we're only monitoring (MONITOR_ONLY=true), jump off here.
                    // eslint-disable-next-line no-await-in-loop
                    const monitorOnly = await checkParams(thisLink, 'MONITOR_ONLY', '');
                    if (monitorOnly === 'true') {
                        message2(`Link: ${linkName}, type: ${thisType}, speed: ${formatBytes(currentSpeed)} - Only performing monitoring - Nothing else to do.`);
                    } else if (currentSpeed < targetSpeed) {
                        message2(
                            `${thisType} link (${stationHostName}), type: ${thisType}, is running slower (${formatBytes(currentSpeed)}) than our minimum (${formatBytes(targetSpeed)}) - I want to DISABLE the port`,
                        );
                        // eslint-disable-next-line no-await-in-loop
                        await setPort(
                            thisLink, linkName, true,
                        ); // true is 'set this port to disabled.
                    } else {
                        message2(
                            `${thisType} link (${stationHostName
                            }) is faster (${formatBytes(currentSpeed)
                            }) than our minimum (${formatBytes(targetSpeed)
                            }) - I want to ENABLE the port`,
                        );

                        // eslint-disable-next-line no-await-in-loop
                        await setPort(
                            thisLink, linkName, false,
                        ); // false is 'set this port to enabled'
                    }
                }
            }
        }
    } catch (error) {
        logError(`poll: Error: ${error.stack}`);
        await sleep(1000);
        process.exit(1);
    }

    // And exit stage left. Give it a second to flush the logs.
    debug('Finishing normally');
    await sleep(1000);
    process.exit(0);
}

// Main entry point. Run our polling process.
poll();
