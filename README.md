# README #

MUGR - Make Ubiquiti Go Right. An Opinionated CARRIER DROP.

### What is MUGR for? ###

* MUGR allows you to disable or enable a VLAN port on a Mikrotik router, 
  depending on the speed of an AF60LR, or a Mikrotik W60G link. 
* **Carrier Drop** for Dummies, really.
* It relies on Node, and a few NPM packages. See [package.json](./package.json)
  
### Version 2 adds ###
  * Added polling devices via **SNMP**
  * Added **Graylog** logging
  * Added **Mimosa** B5 Radios
  * Override log message text and format
  * Save speed-objects to a text file (for those non-Splunkians out there)
  * Multiple links in one configuration file
  * Logging to **Splunk**, for both:
    * Debug, status and error messages
    * Graphing information about the speed of the link over time.
  * Scanning Mikrotik **W60G** PTP access points as well as AF60LR
  * Using an INI file instead of environment variables
  * Monitoring only - handy for just colllecting stats

### What are we trying to fix?

Imagine the following network:

1. Monitoring PC 
2. Mikrotik Router A
3. AF60LR AP (or a Mikrotik W60G)
4. AF60LR STA (or a Mikrotik W60G)
5. Mikrotik Router B

So far so good. Let us also assume there is another link between A and B, and we are using Mikrotik OSPF to control networking
The Good Folks at Ubiquiti and Mikrotik have both done excellent jobs on the AF60LR and W60G 
devices - however, they have forgotten to add something called 'carrier drop'. 
This allows the radio to disable its ethernet port to flag low speed, etc, and 
tells OSPF NOT to use the link if the speed dips.

As you can imagine, during bad weather, the dish will drop out quite a bit, 
and it will take OSPF 15 or so seconds to realise that its dropped.
And during the period just before it drops, thoughput will be rubbish.

So we would like to chop the link before it dies.

Update: The AF60LR firmware v2.5 has been released in late July 2021, and now includes CARRIER DROP! See: [UBNT Firmware Release 2.5](https://community.ui.com/releases/AF60-AF60-LR-Firmware-v2-5-0/98f2520c-8a47-4b7f-83b1-6fc1ec45615e)

### How are we going to update OSPF

* We are not. We are going to disable the VLAN port on the router that the STATION plugs into.
That will tell OSPF.
* Why not the access point end? We will not be able to log into the AP to see the speed 
when the link improves.
* Why not reset the OSPF distances? We do not want to make simultaneous changes to two routers at the same time.
What happens when router A change works, and router B doesnt? Carnage. That is what happens.

### How do I get set up? ###

1. Install Node, and NPM on the target machine.
1. Clone this repository to some directory (by default, 'mugr')
1. Edit the index.ini file.
1. In the mugr directory, run '**npm i**' to download and install the node npm packages. Note that every time I update MUGR, you will need to run this to pick up new packages I have added (For instance, 'graylog2' added in release 2.4.0. This also updates packages to the version I set on each release)
1. Run '**runCron.sh**' to execute it (in *nix, MacOs). It just runs 
  
        node.js ./index.js > mugr.log 2>mugr.err
  
1. Run it using crontab (use 'crontab -e' to edit crontab, and 'crontab -l' to list) every minute using:
  
        * * * * * /bin/sh /[directory]/mugr/runCron.sh

**Notes**

  * You can run it more frequently if you like, but it might start to affect
  the wee CPU on the radio access point. Instead of more frequent, why not
    set the MIN_SPEED higher so it closes the link if it is below 
    RX rate 2, instead of RX rate 1 (around 250Mbs for AF60LR)?
    
* it currently logs to its own directory - this is controlled in the runCron.sh script file. After 2 months, my log directory hit 2gb, so you might want to to keep an eye on that.

### Example run with Debugging ###

* Normal debugging looks like:


        2021-02-06T15:00:59.287Z: AF60LR link (BCS-LR-01-C10011-C347) is faster (658526) than our minimum (125000) - I want to ENABLE the port 
        2021-02-06T15:00:59.766Z: Port: vlan-929 OSPF DISABLED=false on router: Line-00-C347-Brigadoon and we want DISABLED=false - Nothing to do.


* This is what an example run with debugging enabled. The stuff that is indended are 
debug-level messages. Switching debug off means they do not
get written to the log. Note that passwords are **not** logged.
  
        node ./index.js
        2021-02-09T09:40:56.823Z:     "config" section: {"# Set DEBUG":"on to show lots more information","DEBUG":"on","SHOW_STATION_STATS":"false","SLACK_TOKEN":"mySlackToken","SLACK_CHANNEL":"#alerts","SPLUNK_TOKEN":"mySplunkToken","SPLUNK_HOST":"https://mySplunkServer:8088/services/collector/event"}
        2021-02-09T09:40:56.828Z:     "default" section: {"SOURCE_USERNAME":"admin","SOURCE_PASSWORD":"myRadioPassword","TARGET_USERNAME":"admin","TARGET_PASSWORD":"myRouterPassword","MIN_SPEED":"150000","SOURCE_TYPE":"AF60LR","TARGET_FORCE":"false"}
        2021-02-09T09:40:56.832Z: Logging to splunk server on: https://mySplunkServer:8088/services/collector/event
        2021-02-09T09:40:56.833Z: Processing Link: Bridadoon
        2021-02-09T09:40:56.833Z:     link configuration: {"SOURCE_IP":"10.20.30.40","SOURCE_TYPE":"AF60LR","TARGET_IP":"10.20.30.41","TARGET_INTERFACE":"vlan-929 OSPF"}
        2021-02-09T09:40:56.833Z:     Processing INI object: Brigadoon: {"SOURCE_IP":"10.20.30.40","SOURCE_TYPE":"AF60LR","TARGET_IP":"10.20.30.41","TARGET_INTERFACE":"vlan-929 OSPF"}
        2021-02-09T09:40:56.835Z:     SSH Config: {"host":"10.20.30.40","username":"admin","password":"myRadioPassword","readyTimeout":15000,"compress":false}
        2021-02-09T09:40:57.497Z:     AF60LR hostname: BCS-LR-01-C10011-C347
        2021-02-09T09:40:57.497Z:     Returning: speed: 1751750
        2021-02-09T09:40:57.498Z:     logSplunkObject: Sending object: {"linkName":"Bridadoon","AP":"BCS-LR-01-C10011-C347","type":"AF60LR","speed":1751750,"ssid":"Marykirk.com-Wisp334","frequency":70200}
        2021-02-09T09:40:57.498Z:   AF60LR link (BCS-LR-01-C10011-C347) is faster (1751750) than our minimum (150000) - I want to ENABLE the port
        2021-02-09T09:40:57.954Z:     Logged into router: Line-00-C347-Brigadoon
        2021-02-09T09:40:57.954Z:     setPort: Running command: :put [/interface vlan get [find name="vlan-929 OSPF"] disabled]
        2021-02-09T09:40:58.038Z:     setPort: Current DISABLED status is: false (false)
        2021-02-09T09:40:58.038Z:   Port: vlan-929 OSPF DISABLED=false on router: Line-00-C347-Brigadoon and we want DISABLED=false - Nothing to do.
        2021-02-09T09:40:58.038Z: Processing Link: LittleTodday
        2021-02-09T09:40:58.038Z:     link configuration: {"SOURCE_IP":"10.20.40.50","SOURCE_TYPE":"W60G","TARGET_IP":"10.20.40.51","TARGET_INTERFACE":"vlan-941 OSPF"}
        2021-02-09T09:40:58.038Z:     Processing INI object: LittleTodday: {"SOURCE_IP":"10.20.40.50","SOURCE_TYPE":"W60G","TARGET_IP":"10.20.40.51","TARGET_INTERFACE":"vlan-941 OSPF"}
        2021-02-09T09:40:58.039Z:     SSH Config: {"host":"10.20.40.50","username":"admin","password":"MyRadioPassword","readyTimeout":15000,"compress":false}
        2021-02-09T09:40:58.782Z:     W60G hostname: BCS-GW-M-27-C510-C232
        2021-02-09T09:40:58.894Z:     Broken down string: 2.3Gbpsto: 2.3 and: Gbps, and produced number: 2411724.8 Kbps
        2021-02-09T09:40:58.894Z:     logSplunkObject: Sending object: {"linkName":"LittleTodday","type":"W60G","AP":"BCS-GW-M-27-C510-C232","speed":2411724.8}
        2021-02-09T09:40:58.895Z:   W60G link (BCS-GW-M-27-C510-C232) is faster (2411724.8) than our minimum (150000) - I want to ENABLE the port
        2021-02-09T09:40:59.382Z:     Logged into router: Line-00-C232-Wight
        2021-02-09T09:40:59.382Z:     setPort: Running command: :put [/interface vlan get [find name="vlan-941 OSPF"] disabled]
        2021-02-09T09:40:59.471Z:     setPort: Current DISABLED status is: false (false)
        2021-02-09T09:40:59.471Z:   Port: vlan-941 OSPF DISABLED=false on router: Line-00-C232-Wight and we want DISABLED=false - Nothing to do.

### index.ini - the configuration file ###

Remember that feeling when you first edited your config.sys file? (If not, ask your parents). 
Yup. Ini files are old. But hey. They are simple and fast.  It is just a text file with section headings in square brackets. Here is the default: [index.ini](./index.ini)

[config] is where we define global things such as

  * Debugging on or off
  * Show station stats - do we dump the station information to the log. 
    Upside. Gives you a history. Downside. Large logs.
  * Whether or not we want to show messages in [Slack](https://www.slack.com). 
    If so, define your Slack app token, channel and message overrides.
    Hint: I spent more time fighting with the [Slack App](https://slack.com) 
    application to get the token than I did writing code
  * Whether or not you want to save the speed objects in a separate file
  * Splunk. We use [Splunk](https://www.splunk.com), and this allows us to both log all our
    stuff to Splunk, and to create stsatistical graphs in Splunk. 
    Again, define your splunk host and token. If they are not defined, 
    we wont try and log to splunk.
  * Graylog. We are migrating to [Graylog](https://www.graylog.org),
    and we log comments and starts to Graylog. 
    You need to define your Graylog host, and whether or not you want to log graph information.
    
[default] is where we can define default information for the following links.

[MyLinkName] helps us define each link. Replace MyLinkName with
something meaningful to you as we will use that link name in debugging messages and graphs.
The link name HAS TO BE UNIQUE else it will join the non-unique entries together.
Do not cross the beams. Specify: 
(either in the link or in defaults):

  * The Radio link:
    * SOURCE_IP - the IP address of the access point on the link
    * SOURCE_USERNAME - the username you want to use to log into the radio
    * SOURCE_PASSWORD - the password you want to use to log into the radio
    * SOURCE_PORT - the port number SSH should attempt to connect on.

    * SOURCE_TYPE. Type can be either 
        * AF60LR - such as:
            * [Ubiquiti AF60LR](https://store.ui.com/collections/operator-airfiber/products/airfiber-60-lr)
            * [Ubiquiti Gigabeam LR](https://store.ui.com/collections/operator-airmax-devices/products/gbe-lr)
            * It **might** work on other 60Gig Ubiquiti devices, but I have not tested them.
            * The Gigabeam LR already has a 5.5Gig backup link already, so it is 
            unlikly you would want to switch OSPF off and on. Hence I have added a 
              MONITOR_ONLY flag such that it does not do the router stuff. 
              Handy if you want to collect stats, etc.
            * I return the device type in the "Platform" field of the splunk object so you can differentiate between them.
        * W60G - such as:
            * [MikroTik LHG-60G](https://mikrotik.com/product/lhg_60g)
            * [Mikrotik WAP-60G](https://mikrotik.com/product/wap_60g)
            * Note that we only support PTP links. It **might** work on other Mikrotik 60Gig devices - we have not tested them.
        * B5 - Mimosa B5 and B5 Lites.
            * Note that you have to enable "https" on these devices, and on the B5-Lite, enable the "Rest Management" interface.
        * SNMP - Monitor devices via SNMP. You have to define which OIDs you wish to monitor. See the example INI file
        * If you want more devices supported - email me. I like single-malt whisky. 
        Or indeed some Old Pappys reserve.

    * MIN_SPEED - The minimum speed that this link should run. Anything below
    this speed (including disconnected) - and we will switch OFF the station VLAN interafce on the router.
      
    * MONITOR_ONLY - if this is set to "true" we wont attempt to do the router phase. 
    Handy for dumping stats only. See the note above about Gigabeams, for instance.

  * The router:
    * TARGET_IP - the IP address of the rouoter
    * TARGET_USERNAME - the username you want to use to log into the router
    * TARGET_PASSWORD - the password you want to use to log into the router
    * TARGET_PORT - the ssh port we should try to connect to.
    * TARGET_INTERFACE - the VLAN interface representing the station
      end of the radio link.
      
In my (bad!) example, I am using the same username and password for my radios and routers, 
so I can define them in the [default] section, and not have to bother defining them 
in each link section. 

        
        [config]
        # Set DEBUG=on to show lots more information, anything else to disable
        DEBUG=off
        
        # Optional stuff
        # Set to true to show us the statistics object from the station. Warning: Big Logs!
        SHOW_STATION_STATS=false

        # Optional. Dump our station stats JSON objects to a file. 
        # Does not require SHOW_STATION_STATS to be on.
        LOG_STATION_STATS_TO_FILE=stationstats.txt
        
        # Set your Slack application token here, from https://api.slack.com/apps/ ..
        # Leave it blank and we we wont attempt to use Slack.
        SLACK_TOKEN=
        
        # And the Slack channel we will write to
        SLACK_CHANNEL=#alerts

        # Three messages - sent when enabling the link, disabling the link, or when we cannot enable/disable the router port.
        # There are various run time variables you can embed in the message, using the format ${variablename}. The variables are:
        # - vlanInterface - the name of the vlan interface
        # - currentStatus - what the status of the DISABLE flag is, at the start of the operation
        # - hostname - the name of the router as we called it in index.ini
        # - stationHostName - The name of the router via the /system identity command
        # - currentSpeed - the carrier speed of the radio as a string with Bytes, Kb, Mb, etc. 
        #                  The Ubiquiti radios are the combined up/down, the Mikrotik is combined up/down
        #                  and the Mimosa is the Tx from the access point.
        # - newFlag - "on" means we just enabled the vlan port, "off" means we just disabled the vlan port.
        SLACK_ENABLE_LINK_MESSAGE=link: ${stationHostName} has been ENABLED on  ${hostname}, port: ${vlanInterface}, speed: ${currentSpeed} 
        SLACK_DISABLE_LINK_MESSAGE=link: ${stationHostName} has been DISABLED on  ${hostname}, port: ${vlanInterface}, speed: ${currentSpeed} 
        SLACK_FAIL_MESSAGE=link: ${stationHostName} - failed to run command on: ${hostname}, port: ${vlanInterface}, speed: ${currentSpeed} Port: ${vlanInterface} DISABLED=${currentStatus} on router: ${hostname} for link: ${stationHostName} running speed: ${currentSpeed} FAILED to set: ${newFlag}
        
        # Obviously change this URL to point at your splunk server..
        SPLUNK_HOST=https://MySplunkServer.com:8088/services/collector/event
        # define the host and HTTPS data import token for your Splunk HTTPS input.
        SPLUNK_TOKEN=

        # If we want to log the time-series information
        # from the radios to splunk, make this true.
        SHOW_SPLUNK_GRAPHING_OBJECT=false

        # define the graylog host 
        GRAYLOG_HOST=mygraylogserver.brigadoon.com

        # If we want to log the time-series information
        # from the radios to graylog, make this true.
        SHOW_GRAYLOG_GRAPHING_OBJECT=false

        [default]
        # If we set these defaults here, we don not have to supply them on each link.
        
        # Source Radio
        SOURCE_USERNAME=admin
        SOURCE_PASSWORD=MyStationPassword
        # SOURCE_TYPE is the type of radio used in the link. Use either
        # - AF60LR (Ubiquiti AF60, Gigabeam),
        # - W60G (Mikrotik Wireless Wire)
        # - B5 (Mimosa B5, B5-Lite)
        SOURCE_TYPE=AF60LR
        # Min Speed in Kbps. 150000 is 150Mb/s, for instance.
        MIN_SPEED=150000
        
        # Target Router
        TARGET_USERNAME=admin
        TARGET_PASSWORD=MyRouterPassword
        # TARGET_FORCE ignores whether or not the port is disabled, and does the action anyway.
        # Useful for proving the command works.
        TARGET_FORCE=false
        
        # This allows you to override for all (by defining here) or for each
        # (by defining in each link section) which ssh port the subject of our
        # interest is waiting on. There is no code default, so leave these 
        # here. 
        SOURCE_PORT=22
        TARGET_PORT=22


        # Instead of spraying SNMP OID values throughout the file, define them here
        # I've named mine OID_<Manufacturer>_<Device>_<type>_<Unit>, which
        # is probably a good starting point.
        # For Mikrotik W60G, see: https://wiki.mikrotik.com/wiki/Manual:Interface/W60G#SNMP_OIDs_for_monitoring
        # The last letter indicates the unit of measure: EG
        # ..._B - Bytes
        # ..._K - Kilobytes
        # ..._M - Megabytes
        # ..._G - Gigabytes
        OID_MIKROTIK_W60G_CARRIER_K=1.3.6.1.4.1.14988.1.1.1.8.1.13.1

        # This represents a Siklu AH2500FX Interface 2 - the radio.
        OID_SIKLU_CARRIER_B=1.3.6.1.2.1.2.2.1.5.2

        # Our SNMP Community name
        SOURCE_SNMP_COMMUNITY=public


        # Now define each radio link/router port pair.
        # Each Section represents a different link. We will use the section name in our logs.
        # Each link is attempted in the same order that they appear here.        

        [Brigadoon]
        # The Radio IP and type
        SOURCE_IP=10.20.30.40
        # So we are overriding the default SOURCE_TYPE for this link
        SOURCE_TYPE=AF60LR
        # The router IP and interface
        TARGET_IP=10.20.30.41
        TARGET_INTERFACE=vlan-929
        
        [LittleTodday]
        SOURCE_IP=10.30.20.40
        # So we are overriding the default SOURCE_TYPE for this link
        SOURCE_TYPE=W60G
        TARGET_IP=10.30.20.41
        TARGET_INTERFACE=vlan-941

        [BarnardsCastle]
        SOURCE_IP=10.40.50.10
        SOURCE_TYPE=B5
        MONITOR_ONLY=true

        [AuchterTurra]
        SOURCE_IP=10.50.60.70
        SOURCE_TYPE=SNMP
        # Instead of spraying OIDs all over the file, this references a INI file variable you can define
        # in the [default] section.
        SOURCE_OIDS=OID_MIKROTIK_W60G_CARRIER
        MONITOR_ONLY=true

## Who do I talk to? ##

* Bill Buchan - bill@marykirk.com
* We run [Marykirk.com](https://www.Marykirk.com) - a medium sized WISP based in Marykirk, Scotland.
* Marykirk is famous for the first electricity generating wind turbine . In the world. 
* Marykirk has an excellent pub. 

## Example speed objects logged to file ##

If you enable: **LOG_STATION_STATS_TO_FILE**, I will create a file that looks like:

          {
            "linkName":"Brigadoon",
            "AP":"BCS-LR-01-C10011-C347",
            "type":"AF60LR",
            "platform":"airFiber 60 LR",
            "speed":1751750,
            "ssid":"Marykirk.com-Wisp334",
            "frequency":70200,
            "on":"2021-04-16T08:50:40.785Z"
          }
          {
            "linkName":"LittleTodday",
            "AP":"BCS-GB-01-C10039-C232",
            "type":"AF60LR",
            "platform":"GigaBeam LR",
            "speed":1644417.250304,
            "ssid":"Marykirk.com-Wisp317",
            "frequency":58320,
            "on":"2021-04-16T08:50:42.028Z"
          }

Note: Managing this file is up to you. I do not check length, rollover, etc. 

## Splunk ##

[Splunk](https://www.splunk.com) is the daddy of all syslog munching tools. I quite like it
as it is an absolute doddle to get stuff out of. Oh, I know the cool kids are banging on 
about greylog, zabbix and Grafana, but if it is not broke...

Setting up Splunk is something like:

1. Set up a new HTTPS data input on your Splunk server
1. Get the Key and the input URL - put those in the .ini file
1. Run MUGR a few times. Every time MUGR runs, it will put 
   some statistical information into a record, and throws that
   at Splunk. You can see the MUGR index.ini section name - Brigadoon
   in the record. That might help you differentiate (in a nice way) between 
   links. You should see inputs like these:


        { 
            message: { 
                AP: BCS-LR-01-C10011-C347       // Access Point device name
                frequency: 70200                // Radio Frequency
                linkName: Brigadoon             // The link name from index.ini
                platform: Gigabeam LR           // What the device calls itself.
                speed: 1751750                  // the capacity or the speed of the link
                ssid: MI5 Surveilance Van       // The SSID name
                type: AF60LR                    // What kind of generic radio type - AF60LR or W60G
            }
            severity: info
        }
        Show as raw text
        host = mySplunkServer:8088 source = http: monitoring source type = log2metrics_json speed = 1751750


1. Create a search using your input HTTP data input parameters. I developed this 
   search using Splunk: "Smart Mode" (not "Fast Mode") - this appears to correctly
   deconstruct the JSON structure. 
   
   Remember, in dashboard edit mode, you can increase the height of your graph by grabbing 
   the bottom (ooer missus!) of the graph and dragging down.
   
   The "by" command breaks the graph into different lines - one for each link - and the "limit" 
    command allows more than 10 lines in your graph. In this case, I have picked 30. "limit=0" means 
   "just draw them all"
   
    Mine looks like:

        sourcetype=log2metrics_json "message.type"="AF60LR"
        | timechart span=2m max("message.speed") by "message.linkName" limit=30
   
1. My graph looks like this: ![Graph](./graph.png)

## Graylog

Splunk has finally broken me. Their 500mb/day "free" license bit my ass when a machine started logging lots of stuff, we didnt notice and we lost logging. So that is what I get for being cheap, not monitoring logging, etc.

This implementation (using the npmjs.org package "graylog2") passes log messages back via the Graylog GELF UDP input mechanism using port 12001. So you will need to set that up. 

I have also set the "facility" field to "mugr" which makes it easy to search. Levels:

* Debug messages are level 9
* Most messages are level 6
* Error messages are level 3

If you want to visualise the graphs (and why wouldnt you!) - set up an Extractor on that input. This will parse the JSON object into variables. I created an Extractor on the GELF UDP input:

* Type: JSON
* Source: full_message
* Flatten Structures - tick
* List item separator: ,
* Key separator: _
* Key/Value separator: =
* Key Prefix: \<Blank\>
* Key Whitespace replacement: _
* Condition: Only attempt extraction if field contains string: {

And you should end up with all the JSON variables extracted. 

So I set up my graphs using: 

* Search String: facility:mugr AND (AP != '') AND (linkName != '') AND (level: 6) AND (type: W60G)
* Visualisation: Line Chart
* Rows: timestamp
* Columns: linkName
* Sorting: timestamp
* Direction: Ascending
* Metrics: avg(speed)

As you can see, I have separated by Type (B5 = Mimosa, W60G == Mikrotik and AF60LR == Ubiquiti) as clumping more than 10 or so together gets very fussy.

 My graph looks like this: ![Graph](./Graylog-AF60LR-Graphs.png)
## License

**MIT License**

Copyright 2021 COPYRIGHT Bill Buchan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

* It is all for free. Have at it. Buy me a beer if it helps. Or better still, a single malt. Anything over 12 years old. 
* Ubiquiti, Mikrotik, Splunk, Mimosa/AirStream, Graylog and Slack obviously are their own companies/organisations, and I do not in 
  any way, shape or form represent them, or their opinions.
* All mistakes are my own.
* If it creates havoc, sorry. 
  Tell me about it and I will try and fix the code. 
  But the havoc is all your own.
* If you want me to build a more specialised version, I will charge you money.

### Version history ###
* 2.5.2 - 30th August 2021: Added SNMP units of measure to the OID specification string, allowing SNMP to return Bytes, etc, and we properly transform them into something sensible. 
* 2.5.1 - 11th August 2021: Added linkName to error messages, allowing you to quantify which link is throwing errors. 
* 2.5.0 - 1st August 2021: Added **SNMP** as a 'type' allowing you to query any SNMP target.
* 2.4.0 - 30th July: Added **GRAYLOG** logging
* 2.3.3 - 12th July: Added the ability to specify the SSH port, either as a default for both SOURCE and TARGET, or for each system.
* 2.3.2 - 17th June: If the interface name is incorrect, it defaults to 'interface==enabled'. Now complains if the interface on the Mikrotik cannot be found (via error log and slack)
* 2.3.1 - 19th April: Bug with the SLACK_ENABLE/DISABLE messages. Always sending the ENABLE. What an optimist. Fixed.
* 2.3 - 16 Apr 2021: Added **Mimosa** B5 as a SOURCE_TYPE in order to read speed from Mimosa B5 and B5-Lite (tested). Might work with others.
* 2.22 -16 Apr 2021: Added the ability to override the default Slack message with your own, possibly less incomprehensible, message. Added ability to log speed stats to file (as JSON objects).
* 2.21 - 15 Apr 2021: Code cleanup. The Brothers Wisp just highlighted this on the book of face. Gulp. Hope the Ubiquiti guys have a sense of humour. Wonder if they will get rid of the forum post where they accuse me of being a troll?
* 2.2 - 10 Feb 2021: Added 'MONITOR_ONLY' flag and added support for Ubiquiti Gigabeam LRs.
* 2.1 - 9 Feb 2021: Added **Splunk** logging
* 2.0 - 8 Feb 2021: Moved to INI file, added support for Mikrotik **W60G** devices.
* 1.1 - 6 Feb 2021: Added **Slack** notifications and fixed bug with Mikrotik 'disable interface' command
* 1.0 - 6 Feb 2021: First release. 

